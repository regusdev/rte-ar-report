﻿using System;
using CSTSReportGenerator;
using RTEARReportGenerator;
using Logging;
using Quartz;
using Quartz.Impl;
using Topshelf;
using Topshelf.HostConfigurators;
using Topshelf.Runtime;
using Topshelf.ServiceConfigurators;

namespace RTEARReportGenerator
{
    public class ReportScheduler
    {
        private ITrigger _trigger;

        public void Start()
        {
            // construct a scheduler factory
            ISchedulerFactory schedFact = new StdSchedulerFactory();

            // get a scheduler
            var sched = schedFact.GetScheduler();
            sched.Start();

            // define the job and tie it to our HelloJob class
            var job = JobBuilder.Create<ReportRunner>()
                .WithIdentity("myJob", "group1")
                .Build();

            var twoMinutesInTheFuture = DateTime.Now.AddMinutes(2);

            if (Environment.UserInteractive)
            {
                _trigger = (ISimpleTrigger)TriggerBuilder.Create()
                    .WithIdentity("trigger1", "group1")
                    .StartAt(twoMinutesInTheFuture) // some Date
                    .ForJob("myJob", "group1") // identify job with name, group strings
                    .Build();
            }
            else
            {
                //cron trigger for 02:17 daily

                _trigger = TriggerBuilder.Create()
                    .WithIdentity("DailyTrigger", "group1")
                    .WithCronSchedule("0 02 02 * * ?")
                    .ForJob("myJob", "group1")
                    .Build();

                //_trigger = (ISimpleTrigger)TriggerBuilder.Create()
                //    .WithIdentity("trigger1", "group1")
                //    .StartAt(twoMinutesInTheFuture) // some Date
                //    .ForJob("myJob", "group1") // identify job with name, group strings
                //    .Build();
            }


            sched.ScheduleJob(job, _trigger);
        }

        public void Stop()
        {
            Console.WriteLine("scheduler for RTE AR daily report stopped");
        }
    }

    public class Program
    {
        public static void Main()
        {
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            HostFactory.Run(ConfigureCallback);
        }

        private static void ConfigureCallback(HostConfigurator hostConfigurator)
        {
            hostConfigurator.Service<ReportScheduler>(Callback);
            hostConfigurator.RunAsLocalSystem(); //6

            hostConfigurator.SetDescription("Report Scheduler Host");
            hostConfigurator.SetDisplayName("RTE AR Daily Report scheduler");
            hostConfigurator.SetServiceName("RTEARREPORT");
        }

        private static void Callback(ServiceConfigurator<ReportScheduler> s)
        {
            s.ConstructUsing(ServiceFactory); //3
            s.WhenStarted(StartService); //4
            s.WhenStopped(StopService); //5
        }

        private static ReportScheduler ServiceFactory(HostSettings name)
        {
            return new ReportScheduler();
        }

        private static void StopService(ReportScheduler reportScheduler)
        {
            reportScheduler.Stop();
            new Logger().Log("RTE AR Report Service Stopped:", 0);
        }

        private static void StartService(ReportScheduler reportScheduler)
        {
            reportScheduler.Start();
            new Logger().Log("RTE AR Report Service Stopped:", 0);
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            new Logger().Log("-Global unhandled exception:" + e.ExceptionObject, 0);
            // Log exception / send notification.
        }
    }
}