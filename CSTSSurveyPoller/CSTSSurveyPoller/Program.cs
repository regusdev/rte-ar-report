﻿using EmailOperations;

namespace CSTSSurveyPoller
{
    class Program
    {
        static void Main(string[] args)
        {
            var connection = Office365EmailConnector.ConnectHelper();
            var messages = Office365EmailConnector.GetMyMessages(connection);
        }
    }
}
