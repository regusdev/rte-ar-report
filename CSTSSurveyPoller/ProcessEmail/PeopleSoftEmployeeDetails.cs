﻿using System;

namespace ProcessEmail
{
    public class PeopleSoftEmployeeDetails
    {
        public string StartDate { get; set; }
        public DateTime StartDateTime { get; set; }
        public string LeaveDate { get; set; }
        public DateTime LeaveDateTime { get; set; }
        public string Forename { get; set; }
        public string Surname { get; set; }
        public string PreferredName { get; set; }
        public string PreferredEmailAddress { get; set; }
        public string Preferredemail { get; set; }
        public int EmployeeID { get; set; }
        public string JobRole { get; set; }
        public int CentreNumber { get; set; }
        public string CentreName { get; set; }
        public string CentrePhoneno { get; set; }
        public string Supervisor { get; set; }
        public string Supervisoremail { get; set; }
        public string Supervisorphoneno { get; set; }
        public string Network { get; set; }
        public string OutlookEmailaccount { get; set; }
        public string CentreMailBoxaccount { get; set; }
        public string CentreSharedrive { get; set; }
        public string Opsdrive { get; set; }
        public string IntranetSMART { get; set; }
        public string IntranetResource { get; set; }
        public string PivotalincCitrix { get; set; }
        public string Titan { get; set; }
        public string Mars { get; set; }
        public string SecureRemoteVPN { get; set; }
        public string Emaildistributionlists { get; set; }
        public string Blackberry { get; set; }
        public string NewDetails { get; set; }
        public string EmailForward { get; set; }
        public string EmailSubject { get; set; }
        public string EmailFrom { get; set; }
        public string ProcessType { get; set; }
        public string InboundId { get; set; }
        public string LeaverName { get; set; }
    }
}