﻿using System.Collections.Generic;

namespace ProcessEmail
{
    internal class SamanageEmployeeRequest
    {
        public class RequestVariablesAttribute
        {
            public string name { get; set; }
            public string kind { get; set; }
            public string options { get; set; }
            public string required { get; set; }
            public object sorted { get; set; }
            public string uuid { get; set; }
            public string value { get; set; }
        }

        public class Incident
        {
            public string requester_name { get; set; }
            public int site_id { get; set; }
            public List<RequestVariablesAttribute> request_variables_attributes { get; set; }
        }
    }
}