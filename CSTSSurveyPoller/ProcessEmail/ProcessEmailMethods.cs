﻿using Logging;
using Newtonsoft.Json;
using SamanageAPI;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ProcessEmail
{
    public class ProcessEmailMethods
    {
        private static readonly SamanageHttpClient Client = new SamanageHttpClient(new SamanageWebConfig());
        private static readonly StringBuilder notificationString = new StringBuilder();
        private readonly Logger _logger;

        public ProcessEmailMethods(Logger logger)
        {
            _logger = logger;
        }

        public PeopleSoftEmployeeDetails GetPeopleSoftEmployeeDetails(string text, DateTime receivedDateTime,
            string from, string subject)
        {

            var peopleSoftEmployeeDetails = new PeopleSoftEmployeeDetails
            {
                EmailFrom = @from,
                EmailSubject = subject
            };
            var lines = text.Split(new string[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);

            foreach (var line in lines)
            {
                try
                {
                    var tokens = line.Trim().Split(':');
                    var propInfo =
                        peopleSoftEmployeeDetails.GetType()
                            .GetProperty(tokens[0].Replace(" ", "").Replace("-", "").Replace("(", "").Replace(")", ""));
                    if (propInfo != null)
                    {
                        if (propInfo.PropertyType.Name == "Int32")
                        {
                            propInfo.SetValue(peopleSoftEmployeeDetails, int.Parse(tokens[1].Trim()), null);
                        }
                        else
                            propInfo.SetValue(peopleSoftEmployeeDetails, tokens[1].Trim(), null);

                        notificationString.AppendLine(line);
                    }
                    else
                    {
                        _logger.Log("peopleSoftEmployeeDetails property not found: " + tokens[0] + " Please check Peoplesoft HR emails", 3);
                    }
                }
                catch (IndexOutOfRangeException)
                {

                }

            }
            return peopleSoftEmployeeDetails;

        }


        public static async Task<string> GetSites()
        {
            try
            {
                var response = await Client.Get("https://api.samanage.com/sites.json").ConfigureAwait(true);
                var json = await response.Content.ReadAsStringAsync().ConfigureAwait(true);
                return json;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        public Task<string> CreateHrRequestTask(PeopleSoftEmployeeDetails employeeDetails)
        {
            var userresponse = GetUserDetailsFromEmail(employeeDetails.Supervisoremail);
            var site_id = 0;
            var servicerequesttype = 0;
            if (userresponse != "[]")
            {
                var samanageSupervisorDetails = JsonConvert.DeserializeObject<List<samanageUser>>(userresponse);
                site_id = samanageSupervisorDetails[0].site != null ? samanageSupervisorDetails[0].site.id : 0;
            }
            var starterRequest = new SamanageEmployeeRequest.Incident
            {
                requester_name = employeeDetails.Supervisoremail,
                site_id = site_id

            };
            var requestVariablesAttributes = new List<SamanageEmployeeRequest.RequestVariablesAttribute>();

            var notificationRequestVariable = new SamanageEmployeeRequest.RequestVariablesAttribute
            {
                name = "Notification",
                kind = "free_text",
                options = "",
                required = "true",
                sorted = null,
                uuid = "",
                value = notificationString.ToString()
            };

            requestVariablesAttributes.Add(notificationRequestVariable);
            starterRequest.request_variables_attributes = requestVariablesAttributes;

            var incidentContentString = JsonConvert.SerializeObject(starterRequest);

            incidentContentString = "{\"incident\":" + incidentContentString + "}";

            servicerequesttype = Convert.ToInt32(employeeDetails.StartDate != null ? ConfigurationManager.AppSettings["Samanage.HRStarterServiceRequestId"] : ConfigurationManager.AppSettings["Samanage.HRLeaverServiceRequestId"]);

            var response = PostIncident(incidentContentString, "catalog_items/" + servicerequesttype + "/service_requests.json");
            notificationString.Clear();
            return response;
        }

        private static async Task<string> PostIncident(string incidentContentString, string method)
        {
            var incidentContent = new StringContent(incidentContentString);
            incidentContent.Headers.ContentType = new MediaTypeWithQualityHeaderValue("application/json");
            try
            {
                var response =
                    await
                        Client.Post(ConfigurationManager.AppSettings["BaseUrl"] + method, incidentContent)
                            .ConfigureAwait(true);
                var json = await response.Content.ReadAsStringAsync().ConfigureAwait(true);
                return json;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        private static string GetUserDetailsFromEmail(string userEmail)
        {
            try
            {
                var response = Client.Get(ConfigurationManager.AppSettings["BaseUrl"] + "users.json?email=" + userEmail);
                var json = response.Result.Content.ReadAsStringAsync().Result;
                return json;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
    }
}