﻿using System.Collections.Generic;

namespace ProcessEmail
{
    internal class samanageUser
    {
        public int id { get; set; }
        public string name { get; set; }
        public bool disabled { get; set; }
        public string title { get; set; }
        public string email { get; set; }
        public string created_at { get; set; }
        public string last_login { get; set; }
        public string phone { get; set; }
        public string mobile_phone { get; set; }
        public Department department { get; set; }
        public Role role { get; set; }
        public string salt { get; set; }
        public List<int> group_ids { get; set; }
        public List<object> custom_fields_values { get; set; }
        public Avatar avatar { get; set; }
        public ReportsTo reports_to { get; set; }
        public Site site { get; set; }

        public class Department
        {
            public int id { get; set; }
            public string name { get; set; }
            public string description { get; set; }
            public int default_assignee_id { get; set; }
        }

        public class Role
        {
            public int id { get; set; }
            public string name { get; set; }
            public string description { get; set; }
            public bool portal { get; set; }
        }

        public class Avatar
        {
            public string type { get; set; }
            public string image_class { get; set; }
            public string sso_image_class { get; set; }
            public string avatar_url { get; set; }
        }

        public class Role2
        {
            public int id { get; set; }
            public string name { get; set; }
            public string description { get; set; }
            public bool portal { get; set; }
        }

        public class Avatar2
        {
            public string type { get; set; }
            public string color { get; set; }
            public string initials { get; set; }
        }

        public class ReportsTo
        {
            public int group_id { get; set; }
            public bool is_user { get; set; }
            public int id { get; set; }
            public string name { get; set; }
            public bool disabled { get; set; }
            public string title { get; set; }
            public string email { get; set; }
            public string created_at { get; set; }
            public string last_login { get; set; }
            public string phone { get; set; }
            public string mobile_phone { get; set; }
            public object department { get; set; }
            public Role2 role { get; set; }
            public string salt { get; set; }
            public List<int> group_ids { get; set; }
            public List<object> custom_fields_values { get; set; }
            public Avatar2 avatar { get; set; }
            public object site { get; set; }
        }

        public class BusinessRecord
        {
            public int id { get; set; }
            public string name { get; set; }
            public string description { get; set; }
        }

        public class Site
        {
            public int id { get; set; }
            public string name { get; set; }
            public string location { get; set; }
            public string description { get; set; }
            public object time_zone { get; set; }
            public object language { get; set; }
            public BusinessRecord business_record { get; set; }
        }
    }
}